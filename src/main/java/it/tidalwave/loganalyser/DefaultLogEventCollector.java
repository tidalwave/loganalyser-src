/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import lombok.Builder;
import static java.util.Comparator.comparing;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
@Builder
public class DefaultLogEventCollector implements LogEventCollector
  {
    private final List<LogEvent> logEvents = new ArrayList<>();

    @Nonnull
    private final Function<LogEvent, String> formatter;

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override
    public void start()
      {
      }

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override
    public void stop()
      {
        logEvents.stream()
                .sorted(comparing(LogEvent::getTimestamp))
                .map(formatter)
                .forEach(System.out::println);
      }

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override
    public void consume (final @Nonnull LogEvent logEvent)
      {
        logEvents.add(logEvent);
      }
  }
