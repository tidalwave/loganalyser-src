/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import it.tidalwave.loganalyser.filter.LogEventFilter;
import it.tidalwave.loganalyser.config.GlobalConfig;
import it.tidalwave.loganalyser.config.LogEventSourceConfig;
import static java.nio.file.FileVisitOption.FOLLOW_LINKS;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
public abstract class LogEventFileSourceSupport implements LogEventSource
  {
    @Nonnull
    private final Path sourceFile;

    @Nonnull
    private final List<LogEventFilter> logEventFilters;

    private final AtomicInteger dotCounter = new AtomicInteger();

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    protected LogEventFileSourceSupport (final @Nonnull LogEventSourceConfig config)
      {
        this.sourceFile = config.getSourceFile();
        this.logEventFilters = config.getFilters();
      }

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override
    public void process (final @Nonnull LogEventCollector collector, final @Nonnull GlobalConfig config)
      {
        try
          {
            final Path folder = sourceFile.getParent();
            final Pattern filePattern  = Pattern.compile(sourceFile.getFileName().toString());
            Files.walk(folder, FOLLOW_LINKS)
                    .filter(path -> filePattern.matcher(path.getFileName().toString()).matches())
                    .peek(file -> System.err.printf("%nProcessing %s ", folder.relativize(file)))
                    .flatMap(this::lines)
                    .peek(line -> printDot())
                    .flatMap(this::parse)
                    .flatMap(event -> annotate(event, config))
                    .forEach(collector::consume);
            System.err.println();
          }
        catch (IOException ex)
          {
            throw new RuntimeException(ex);
          }
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @Nonnull
    protected abstract Stream<LogEvent> parse (@Nonnull String line);

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @Nonnull
    private Stream<LogEvent> annotate (final @Nonnull LogEvent logEvent, final @Nonnull GlobalConfig config)
      {
        if ((logEventFilters == null /** FIXME **/) || logEventFilters.isEmpty())
          {
            return Stream.of(logEvent);
          }

        // FIXME: refactor with lambda
        for (final LogEventFilter filter : logEventFilters)
          {
            final Optional<LogEvent> filtered = filter.apply(logEvent, config);

            if (filtered.isPresent())
              {
                return Stream.of(filtered.get());
              }
          }

        return Stream.empty();
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    private void printDot()
      {
        if (dotCounter.incrementAndGet() % 100 == 0)
          {
            System.err.print(".");
          }
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @Nonnull
    private Stream<String> lines (final @Nonnull Path path)
      {
        try
          {
            return Files.lines(path);
          }
        catch (IOException ex)
          {
            throw new RuntimeException(ex);
          }
      }
  }
