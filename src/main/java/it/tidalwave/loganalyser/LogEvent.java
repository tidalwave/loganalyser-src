/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Wither;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
@Immutable @Builder @Getter @Wither @ToString
public class LogEvent
  {
    public enum Kind
      {
        NORMAL, WARNING, PROBLEM, PROBLEM_SOLVED
      };

    private static final Map<Kind, String> COLOUR_MAP = new HashMap<>();

    private static final String ESC = "\u001b";
    private static final String RESET = ESC + "[0m";
    private static final String YELLOW = ESC + "[33m";
    private static final String RED = ESC + "[31m";
    private static final String GREEN = ESC + "[32m";

    static
      {
        COLOUR_MAP.put(Kind.NORMAL, RESET);
        COLOUR_MAP.put(Kind.WARNING, YELLOW);
        COLOUR_MAP.put(Kind.PROBLEM, RED);
        COLOUR_MAP.put(Kind.PROBLEM_SOLVED, GREEN);
      }

    private static final DateTimeFormatter TIMESTAMP_FORMATTER = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern("dd-MM-yyyy HH:mm:ss.SSS") // 23-07-2017 00:53:36.793
            .toFormatter(Locale.ENGLISH);

    private final LocalDateTime timestamp;

    private final String originalLine;

    private final String level;

    private final String content;

    private final String explanation;

    private final Kind kind;

    @Nonnull
    public String formatted()
      {
        return TIMESTAMP_FORMATTER.format(timestamp) + " " + explanation;
      }

    @Nonnull
    public String formattedInColour()
      {
        return COLOUR_MAP.getOrDefault(kind, "") + formatted() + RESET;
      }
  }
