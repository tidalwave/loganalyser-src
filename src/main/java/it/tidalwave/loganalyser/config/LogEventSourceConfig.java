/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser.config;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.Function;
import java.nio.file.Path;
import it.tidalwave.loganalyser.LogEventSource;
import it.tidalwave.loganalyser.filter.LogEventFilter;
import it.tidalwave.loganalyser.source.AccessLogSource;
import it.tidalwave.loganalyser.source.CatalinaLogEventSource;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.Wither;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
@Builder @Getter @ToString
public class LogEventSourceConfig
  {
    @RequiredArgsConstructor @Getter
    public enum Format
      {
        CATALINA(CatalinaLogEventSource::new),
        ACCESS_LOG(AccessLogSource::new);

        @Nonnull
        private final Function<LogEventSourceConfig, LogEventSource> factory;
      }

    @Nonnull
    private final String sourceFileName;

    @Wither
    private final Path sourceFile;

    @Nonnull
    private final Format format;

//    @Nonnull FIXME
    private final List<LogEventFilter> filters;

    @Nonnull
    public LogEventSource newLogEventSource()
      {
        return format.getFactory().apply(this);
      }
  }
