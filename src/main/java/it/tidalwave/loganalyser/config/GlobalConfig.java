/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser.config;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.Function;
import java.nio.file.Path;
import java.nio.file.Paths;
import com.thoughtworks.xstream.XStream;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.Option;
import it.tidalwave.loganalyser.DefaultLogEventCollector;
import it.tidalwave.loganalyser.LogEvent;
import it.tidalwave.loganalyser.LogEventCollector;
import it.tidalwave.loganalyser.Main;
import it.tidalwave.loganalyser.filter.GenericLogEventFilter;
import lombok.Builder;
import lombok.ToString;
import lombok.experimental.Wither;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
@Builder @ToString
public class GlobalConfig
  {
    @Argument(metaVar = "path", required = true, usage = "path to the directory with log files to analyse")
    private String folderPath;

    @Option(name = "-c", aliases = "--config", metaVar = "path", required = true, usage = "path to the configuration file")
    private String configFilePath;

    @Option(name = "-t", aliases = "--no-colour", required = false, usage = "suppresses coloured output")
    private boolean noColour = false;

    @Option(name = "-h", aliases = "--help", usage = "print this message")
    private boolean help = false;

    private final String[] args;

    @Wither
    private final Path folder;

    @Wither
    private final Function<LogEvent, String> formatter;

//    @Nonnull
    private final List<LogEventSourceConfig> logEventSourceConfigs;

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @Nonnull
    public GlobalConfig configured()
      {
        final CmdLineParser parser = new CmdLineParser(this);

        try
          {
            if (help || (args.length == 0))
              {
                printUsage(parser);
              }

            parser.parseArgument(args);
            final Path configFile = Paths.get(configFilePath);
            System.err.println("Using config file at " + configFile.toAbsolutePath());

            return read(configFile)
                    .withFolder(Paths.get(folderPath))
                    .withFormatter(noColour ? LogEvent::formatted : LogEvent::formattedInColour);
          }
        catch (CmdLineException e)
          {
            System.err.println(e.getMessage());
            printUsage(parser);
            return; // never reached
          }
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    public void run()
      {
        final LogEventCollector collector = DefaultLogEventCollector.builder()
                .formatter(formatter)
                .build();

        collector.start();
        logEventSourceConfigs.stream()
                .map(config -> config.withSourceFile(folder.resolve(config.getSourceFileName())))
                .map(LogEventSourceConfig::newLogEventSource)
                .forEach(source -> source.process(collector, this));
        collector.stop();
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @Nonnull
    private static GlobalConfig read (final @Nonnull Path configFile)
      {
        return (GlobalConfig)newXStream().fromXML(configFile.toFile());
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @Nonnull
    private static XStream newXStream()
      {
        final XStream xStream = new XStream();
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypesByWildcard(new String[]{Main.class.getPackage().getName() + ".**"});
        xStream.alias("filter", GenericLogEventFilter.class);
        xStream.alias("source", LogEventSourceConfig.class);
        xStream.alias("configuration", GlobalConfig.class);
        xStream.aliasAttribute(LogEventSourceConfig.class, "sourceFileName", "file");
        xStream.addImplicitCollection(GlobalConfig.class, "logEventSourceConfigs");
        xStream.addImplicitCollection(LogEventSourceConfig.class, "filters");
        return xStream;
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    private void printUsage (final @Nonnull CmdLineParser parser)
      {
        System.err.println("LogAnalyser usage:\n");
        parser.printUsage(System.err);
        System.exit(0);
      }
  }
