/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser.filter;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import it.tidalwave.loganalyser.LogEvent;
import it.tidalwave.loganalyser.config.GlobalConfig;
import lombok.Builder;
import lombok.ToString;
import static it.tidalwave.loganalyser.LogEvent.Kind.NORMAL;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
@Immutable @Builder @ToString
public class GenericLogEventFilter implements LogEventFilter
  {
    @Nonnull
    private final String regex;

    @Nonnull
    private final String explanation;

    @Nonnull
    @Builder.Default
    private final LogEvent.Kind kind = NORMAL;

    @Override @Nonnull
    public Optional<LogEvent> apply (final @Nonnull LogEvent event, final @Nonnull GlobalConfig config)
      {
        final Pattern pattern = Pattern.compile(regex); // FIXME: do just once
        final Matcher matcher = pattern.matcher(event.getContent());

        return !matcher.matches()
                ? Optional.empty()
                : Optional.of(event.withExplanation(matcher.replaceAll(explanation))
                                   .withKind(kind));
      }
  }
