/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser.source;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import it.tidalwave.loganalyser.LogEvent;
import it.tidalwave.loganalyser.LogEventFileSourceSupport;
import it.tidalwave.loganalyser.config.LogEventSourceConfig;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
public class CatalinaLogEventSource extends LogEventFileSourceSupport
  {
    private static final String TIMESTAMP_PATTERN = "dd-MMM-yyyy HH:mm:ss.SSS";

    private static final String LINE_REGEX = "^(?<timestamp>[0-9A-Za-z-]* [0-9.:]*) "
                                            + "(?<level>[A-Z]*) "
                                            + "(?<content>.*)$";

    private static final DateTimeFormatter CATALINA_TIMESTAMP_FORMATTER = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern(TIMESTAMP_PATTERN)
            .toFormatter(Locale.ENGLISH);

    private static final Pattern CATALINA_LOG_LINE_PATTERN = Pattern.compile(LINE_REGEX);

    private LocalDateTime timestamp;

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    public CatalinaLogEventSource (final @Nonnull LogEventSourceConfig config)
      {
        super(config);
      }

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override
    protected Stream<LogEvent> parse (final @Nonnull String line)
      {
        final Matcher matcher = CATALINA_LOG_LINE_PATTERN.matcher(line);

        if (!matcher.matches())
          {
            return Stream.of(LogEvent.builder()
                    .timestamp(timestamp)
                    .originalLine(line)
                    .content(line)
                    .build());
          }
        else
          {
            timestamp = LocalDateTime.parse(matcher.group("timestamp"), CATALINA_TIMESTAMP_FORMATTER);
            final String level = matcher.group("level");
            final String content = matcher.group("content");

            return Stream.of(LogEvent.builder()
                    .timestamp(timestamp)
                    .originalLine(line)
                    .level(level)
                    .content(content)
                    .build());
          }
      }
  }
