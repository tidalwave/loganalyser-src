/*
 * #%L
 * *********************************************************************************************************************
 *
 * Log Analyser - open source log analyser
 * http://loganalyser.tidalwave.it - git clone https://bitbucket.org/tidalwave/loganalyser-src
 * %%
 * Copyright (C) 2017 - 2017 Tidalwave s.a.s. (http://tidalwave.it)
 * %%
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * $Id:$
 *
 * *********************************************************************************************************************
 * #L%
 */
package it.tidalwave.loganalyser.source;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import it.tidalwave.loganalyser.LogEvent;
import it.tidalwave.loganalyser.LogEventFileSourceSupport;
import it.tidalwave.loganalyser.config.LogEventSourceConfig;
import static it.tidalwave.loganalyser.LogEvent.Kind.*;

/***********************************************************************************************************************
 *
 * @author  Fabrizio Giudici <Fabrizio dot Giudici at tidalwave dot it>
 * @version $Id:$
 *
 **********************************************************************************************************************/
public class AccessLogSource extends LogEventFileSourceSupport
  {
    private static final String TIMESTAMP_PATTERN = "dd/MMM/yyyy:HH:mm:ss +0200"; // 23/Jul/2017:00:00:13 +0200 FIXME: timezone should be ignored

    private static final String LINE_REGEX = "^(?<clientIp>[0-9a-f.:]+).*"
            + "\\[(?<timestamp>.*)\\] "
            + "\"(?<command>.*)\" "
            + "(?<status>[0-9]+) "
            + "(?<size>[0-9]+)";

    private static final DateTimeFormatter ACCESS_TIMESTAMP_FORMATTER = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern(TIMESTAMP_PATTERN)
            .toFormatter(Locale.ENGLISH);

    private static final Pattern ACCESS_LOG_LINE_PATTERN = Pattern.compile(LINE_REGEX);

    private final Map<String, Integer> statusMapByContext = new HashMap<>();

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    public AccessLogSource (final @Nonnull LogEventSourceConfig config)
      {
        super(config);
      }

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override
    protected Stream<LogEvent> parse (final @Nonnull String line)
      {
        Stream<LogEvent> result = Stream.empty();

        final Matcher matcher = ACCESS_LOG_LINE_PATTERN.matcher(line);

        if (matcher.matches())
          {
            final String clientIp = matcher.group("clientIp");
            final LocalDateTime timestamp = LocalDateTime.parse(matcher.group("timestamp"), ACCESS_TIMESTAMP_FORMATTER);
            final String command = matcher.group("command");
            final int httpStatus = Integer.parseInt(matcher.group("status"));
            final String size = matcher.group("size");

            final LogEvent.LogEventBuilder builder = LogEvent.builder()
                    .timestamp(timestamp)
                    .originalLine(line)
                    .kind(NORMAL);

            final String[] split = command.split(" ")[1].trim().split("\\/");
            final String context = (split.length > 0) ? split[1] : "/";
            final int previousContextStatus = statusMapByContext.getOrDefault(context, 200);

            if (previousContextStatus != httpStatus)
              {
                statusMapByContext.put(context, httpStatus);
                result = response(httpStatus, builder, command, context);
              }
          }

        return result;
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @Nonnull
    private static Stream<LogEvent> response (final int httpStatus,
                                              final @Nonnull LogEvent.LogEventBuilder builder,
                                              final @Nonnull String command,
                                              final @Nonnull String context)
      {
        switch (httpStatus)
          {
            case 200:
                return Stream.of(builder
                        .explanation("    '" + context + "' RESUMED RESPONDING 200 (Ok): " + command)
                        .kind(PROBLEM_SOLVED)
                        .build());
            case 404:
                return Stream.of(builder
                        .explanation("    '" + context + "' STARTED RESPONDING 404 (Not Found): " + command)
                        .kind(PROBLEM)
                        .build());
            case 500:
                return Stream.of(builder
                        .explanation("    '" + context + "' STARTED RESPONDING 500 (Internal Error): " + command)
                        .kind(PROBLEM)
                        .build());
            default:
                return Stream.empty();
          }
      }
  }
